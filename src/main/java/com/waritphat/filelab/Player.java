package com.waritphat.filelab;

import java.io.Serializable;

/**
 *
 * @author domem
 */
public class Player implements Serializable{
    private char symbol;
    private int win;
    private int loss;
    private int draw;
    
    Player(char symbol){
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getWin() {
        return win;
    }

    public int getLoss() {
        return loss;
    }

    public int getDraw() {
        return draw;
    }
    
    public void win() {
        this.win++;
    }

    public void loss() {
        this.loss++;
    }

    public void draw() {
        this.draw++;
    }

    @Override
    public String toString() {
        return "Player{" + "symbol=" + symbol + ", win=" + win + ", loss=" + loss + ", draw=" + draw + '}';
    }
    
    
}
